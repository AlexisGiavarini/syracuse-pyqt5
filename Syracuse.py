import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class MyMainWindow(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        self.initUI()

    def clickMethod(self):
        QMessageBox.about(self, "Champ vide", "Veuillez saisir un nombre")

    def update_label(self):
        if self.champ.text() != "":
            self.champ2.setText("")
            n = int(self.champ.text())
            values = [n]

            if n > 1:
                maxi = n
                count = 1

                while n > 1:
                    if n % 2 == 0:
                        n = n / 2
                    else:
                        n = 3 * n + 1

                    count = count + 1
                    if n > maxi:
                        maxi = n
                    values.append(n)

                nb = "nombre de termes dans la suite :" + str(count)
                valMax = "valeur max de la suite :" + str(maxi)

            for v in values:
                self.champ2.append(str(v))

            current_text = "\n" + nb + "\n" + valMax
            self.champ2.append(current_text)

            # Tracé de la courbe
            nb = len(values)
            hmax = max(values)
            x = 0
            painter = QPainter(self.px)
            self.px.fill(QColor(200, 200, 200))
            for i in values:
                x2 = x
                y = self.px.height() - (i * (self.px.height() / hmax))
                painter.drawPoint(x2, y)

                if x > 0:
                    painter.drawLine(x2, y, x1, y2)
                x1 = x
                y2 = self.px.height() - (i * (self.px.height() / hmax))
                x += self.px.width() / nb

            self.lb.setPixmap(self.px)
        else:
            self.clickMethod()

    keyPressed = pyqtSignal()

    def keyPressEvent(self, event):
        super(MyMainWindow, self).keyPressEvent(event)
        if event.key() == Qt.Key_Return:
            self.keyPressed.emit()

    def closeEvent(self, event):
        close = QMessageBox()
        close.setText("Voulez vous vraiment quitter ?")
        close.setStandardButtons(QMessageBox.No | QMessageBox.Yes)
        close = close.exec()

        if close == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def initUI(self):
        self.setGeometry(600, 600, 500, 300)
        self.setWindowTitle('Suite de Syracuse')

        self.label = QLabel("Veuillez saisir un nombre")

        self.champ = QLineEdit()
        self.keyPressed.connect(self.update_label)
        self.onlyInt = QIntValidator(0, 100000)  # N'autorise que les int de 0 à 100000
        self.champ.setValidator(self.onlyInt)

        self.bu = QPushButton("Calculer")
        self.champ2 = QTextEdit()

        self.bu2 = QPushButton("Quitter")
        self.bu.clicked.connect(self.update_label)
        self.bu2.clicked.connect(self.close)  # passe par le closeEvent

        self.lb = QLabel()
        self.lb.setFixedWidth(self.width())
        self.lb.setFixedHeight(self.height())
        self.lb.setFrameShape(QFrame.Panel)

        self.px = QPixmap(self.width(), self.height())

        layout = QVBoxLayout(self)
        layout.addWidget(self.label)
        layout.addWidget(self.champ)
        layout.addWidget(self.bu)
        layout.addWidget(self.champ2)
        layout.addWidget(self.lb)
        layout.addWidget(self.bu2)
        self.setLayout(layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyMainWindow()
    w.show()
    app.exec()